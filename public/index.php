<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);
session_start();
define('PUBLIC_PATH', __DIR__);

use Task\Request;
use Task\Router;

require '../vendor/autoload.php';
require '../core/functions.php';
require '../core/bootstrap.php';
Router::loadRoutes('../web/routes.php')->direct(Request::uri());
