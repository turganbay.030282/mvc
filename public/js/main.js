const bc = new BroadcastChannel('dcode');
bc.addEventListener('message', e => {
    if (e.data === 'logout'){
        location.replace('/login')
    }
})

document.querySelector('#logout-btn').addEventListener('click', e => {
    bc.postMessage('logout')
})
