* git clone git@gitlab.com:turganbay.030282/mvc.git
* cd mvc
* php composer.phar install
* cp .env.example .env (add db name and password)
* php vendor/bin/phinx migrate (php vendor/bin/phinx rollback)
* php vendor/bin/phinx seed:run -s UserSeeder

