<?php

use Task\Controllers\Admin\AdminController;
use Task\Controllers\Admin\TaskController as AdminTaskController;
use Task\Controllers\Admin\UserController;
use Task\Controllers\SecurityController;
use Task\Controllers\TaskController;
use Task\Router;

Router::get('/',  ['controller' => TaskController::class, 'action' => 'index']);

Router::get('tasks',  ['controller' => TaskController::class, 'action' => 'index']);
Router::get('tasks/{id}/show',  ['controller' => TaskController::class, 'action' => 'show']);
Router::get('tasks/create',  ['controller' => TaskController::class, 'action' => 'create']);
Router::post('tasks/store',  ['controller' => TaskController::class, 'action' => 'store']);


Router::get('register',  ['controller' => SecurityController::class, 'action' => 'registerForm']);
Router::post('register',  ['controller' => SecurityController::class, 'action' => 'register']);
Router::get('login',  ['controller' => SecurityController::class, 'action' => 'loginForm']);
Router::post('login',  ['controller' => SecurityController::class, 'action' => 'login']);
Router::get('logout',  ['controller' => SecurityController::class, 'action' => 'logout']);

Router::get('admin',  ['controller' => AdminController::class, 'action' => 'index', 'middleware'=>'admin']);

Router::get('admin/users',  ['controller' => UserController::class, 'action' => 'index', 'middleware'=>'admin']);
Router::get('admin/users/create',  ['controller' => UserController::class, 'action' => 'create', 'middleware'=>'admin']);
Router::post('admin/users/store',  ['controller' => UserController::class, 'action' => 'store', 'middleware'=>'admin']);
Router::get('admin/users/{id}/edit',  ['controller' => UserController::class, 'action' => 'edit', 'middleware'=>'admin']);
Router::post('admin/users/{id}/update',  ['controller' => UserController::class, 'action' => 'update', 'middleware'=>'admin']);
Router::get('admin/users/{id}/delete',  ['controller' => UserController::class, 'action' => 'delete', 'middleware'=>'admin']);

Router::get('admin/tasks',  ['controller' => AdminTaskController::class, 'action' => 'index', 'middleware'=>'admin']);
Router::get('admin/tasks/{id}/edit',  ['controller' => AdminTaskController::class, 'action' => 'edit', 'middleware'=>'admin']);
Router::post('admin/tasks/{id}/update',  ['controller' => AdminTaskController::class, 'action' => 'update', 'middleware'=>'admin']);
Router::get('admin/tasks/{id}/delete',  ['controller' => AdminTaskController::class, 'action' => 'delete', 'middleware'=>'admin']);

