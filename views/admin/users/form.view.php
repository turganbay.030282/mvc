<div class="d-flex bd-highlight p-2">
    <div class="flex-grow-1 bd-highlight">
        <h1><?= $title ?></h1>
    </div>
    <div class="p-2 bd-highlight">
        <a href="/admin/users" title="users">
            <i class="far fa-list"></i>
        </a>
    </div>
</div>
<form action="<?= $action ?>" method="post">
    <div class="form-group p-2">
        <label for="email">Email</label>
        <input type="text" class="form-control" id="email" name="email"
               value="<?php echo isset($user) ? $user['email'] : '' ?>">
        <small class="text-danger form-text"><?= getErrorMessage('emailError') ?></small>
    </div>
    <div class="form-group p-2">
        <label for="login">Login</label>
        <input type="text" class="form-control" id="login" name="login"
               value="<?php echo isset($user) ? $user['login'] : '' ?>">
        <small class="text-danger form-text"><?= getErrorMessage('loginError') ?></small>
    </div>
    <div class="form-group p-2">
        <label for="name">Name</label>
        <input type="text" class="form-control" id="name" name="name"
               value="<?php echo isset($user) ? $user['name'] : '' ?>">
        <small class="text-danger form-text"><?= getErrorMessage('nameError') ?></small>
    </div>
    <div class="form-group p-2">
        <label for="password">Password</label>
        <input type="password" class="form-control" id="password" name="password">
        <small class="text-danger form-text"><?= getErrorMessage('passwordError') ?></small>
    </div>
    <div class="form-group p-2">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</form>