<div class="d-flex bd-highlight">
    <div class="p-2 flex-grow-1 bd-highlight">
        <h1><?= $title ?></h1>
    </div>
    <div class="p-3 bd-highlight">
        <a href="/admin/users/create" title="create user">
            <i style="font-size: 20px" class="fas fa-plus-square"></i>
        </a>
    </div>
</div>

<table class="table">
    <thead>
    <tr>
        <th scope="col">id</th>
        <th scope="col">Email</th>
        <th scope="col">Login</th>
        <th scope="col">Name</th>
        <th scope="col" width="20">Action</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($users['data'] as $key => $user) : ?>
        <tr>
            <td><?= $user['id']; ?></td>
            <td><?= $user['email']; ?></td>
            <td><?= $user['login']; ?></td>
            <td><?= $user['name']; ?></td>
            <td class="d-flex">
                <div class="flex-grow-1">
                    <a href="/admin/users/<?= $user['id'] ?>/edit">
                        <i class="fas fa-pencil-alt"></i>
                    </a>
                </div>
                <div class="flex-grow-1">
                    <a href="/admin/users/<?= $user['id'] ?>/delete">
                        <i class="fas fa-trash-alt"></i>
                    </a>
                </div>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<?php if ($users['pages'] > 1) : ?>
    <nav aria-label="Page navigation example">
        <ul class="pagination">
            <?php for ($i = 1; $i < ($users['pages'] + 1); $i++) : ?>
                <li class="page-item">
                    <a class="page-link" href="/admin/users?page=<?= $i ?>"><?= $i ?></a>
                </li>
            <?php endfor ?>
        </ul>
    </nav>
<?php endif ?>