<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
          integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

    <link rel="stylesheet" href="/css/main.css">
    <title>ADMIN::<?php echo isset($title) ? $title : '' ?></title>
</head>
<body>

<div class="px-3 bg-light">
    <div class="d-flex bd-highlight">
        <div class="p-2 w-100 bd-highlight">
            Admin panel
        </div>
        <div class="p-2 flex-shrink-1 bd-highlight">
            <a href="/">Site</a>
        </div>
        <div class="p-2 flex-shrink-1 bd-highlight">
            <a id="logout-btn" href="/logout">Logout</a>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-2 bg-light p-5">
        <p><a href="/admin">Dashboard</a></p>
        <p><a href="/admin/users">Users</a></p>
        <p><a href="/admin/tasks">Tasks</a></p>
    </div>
    <div class="col-10 p-5">
        <?php if (isset($_SESSION['success'])) : ?>
            <div class="alert alert-success" role="alert"><?= $_SESSION['success'] ?></div>
            <?php unset($_SESSION['success']); ?>
        <?php endif; ?>

        <?php if (isset($_SESSION['error'])) : ?>
            <div class="alert alert-danger" role="alert"><?= $_SESSION['error'] ?></div>
            <?php unset($_SESSION['error']); ?>
        <?php endif; ?>

        <?php include $template; ?>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf"
        crossorigin="anonymous"></script>
<script src="/js/main.js"></script>
</body>
</html>