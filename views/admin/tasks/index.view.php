<div class="d-flex bd-highlight">
    <div class="p-2 flex-grow-1 bd-highlight">
        <h1><?= $title ?></h1>
    </div>
</div>


<table class="table">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">User</th>
        <th scope="col">Email</th>
        <th scope="col">Description</th>
        <th scope="col">Status</th>
        <th scope="col" width="20">Action</th>
    </tr>
    </thead>
    <tbody>

    <?php  foreach ($tasks['data'] as $key => $user) : ?>
        <tr>
            <td><?= $user['id']; ?></td>
            <td><?= $user['name']; ?></td>
            <td><?= $user['email']; ?></td>
            <td>
                <div class="task-text">
                    <a href="/tasks/<?= $user['id'] ?>/show"><?= $user['text']; ?></a>
                </div>
            </td>
            <td>
                <?php if ($user['status']) : ?>
                    <i class="fas fa-check"></i>
                <?php else : ?>
                    <i class="fas fa-minus"></i>
                <?php endif; ?>
            </td>
            <td class="d-flex">
                <div class="flex-grow-1">
                    <a href="/admin/tasks/<?= $user['id'] ?>/edit">
                        <i class="fas fa-pencil-alt"></i>
                    </a>
                </div>
                <div class="flex-grow-1">
                    <a href="/admin/tasks/<?= $user['id'] ?>/delete">
                        <i class="fas fa-trash-alt"></i>
                    </a>
                </div>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<?php if ($tasks['pages'] > 1) : ?>
    <nav aria-label="Page navigation example">
        <ul class="pagination">
            <?php for ($i = 1; $i < ($tasks['pages'] + 1); $i++) : ?>
                <li class="page-item">
                    <a class="page-link" href="/admin/tasks?page=<?= $i ?>"><?= $i ?></a>
                </li>
            <?php endfor ?>
        </ul>
    </nav>
<?php endif; ?>

