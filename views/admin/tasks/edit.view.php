<div class="d-flex bd-highlight">
    <div class="p-2 flex-grow-1 bd-highlight">
        <h1>Task: # <?= $task['id']; ?></h1>
    </div>
</div>
<form action="/admin/tasks/<?= $task['id']; ?>/update" method="post">
    <div class="mb-3">
        <label for="text" class="form-label">Description</label>
        <textarea class="form-control" id="text" rows="2" name="text"><?= $task['text'] ?></textarea>
    </div>
    <select class="form-select mb-3" name="userId">
        <option>Choose user</option>
        <?php foreach ($users as $user) : ?>
            <option <?php if($user['id'] == $task['userId']): echo 'selected'; endif ?> value="<?= $user['id'] ?>"><?= $user['email'] ?></option>
        <?php endforeach; ?>
    </select>
    <div class="form-check mb-3">
        <input class="form-check-input" type="checkbox" id="status" name="status" <?php if($task['status']): echo 'checked'; endif ?>>
        <label class="form-check-label" for="status">
            Completed
        </label>
    </div>
    <div class="my-3">
        <button type="submit" class="btn btn-primary">Edit</button>
    </div>
</form>