
<h1>Task # <?= $task['id']; ?></h1>

<p>
    <?= $task['text']; ?>
</p>
<p>
    Status:
    <?php if ($task['status']) : ?>
        <i class="fas fa-check"></i>
    <?php else : ?>
        <i class="fas fa-minus"></i>
    <?php endif; ?>
</p>

<p>
    User: <?= $task['name']; ?>
</p>
<p>
    Email: <?= $task['email']; ?>
</p>
