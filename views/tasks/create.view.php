<div class="d-flex bd-highlight">
    <div class="p-2 flex-grow-1 bd-highlight">
        <h1>New Task</h1>
    </div>
</div>

<form action="/tasks/store" method="post">
    <div class="mb-3">
        <label for="text" class="form-label">Description</label>
        <textarea class="form-control" id="text" rows="2" name="text"></textarea>
        <small class="text-danger form-text"><?= getErrorMessage('textError') ?></small>
    </div>
    <div class="mb-3">
        <select class="form-select" name="userId">
            <option value="">Choose user</option>
            <?php foreach ($users as $user) : ?>
                <option value="<?= $user['id'] ?>"><?= $user['email'] ?></option>
            <?php endforeach; ?>
        </select>
        <small class="text-danger form-text"><?= getErrorMessage('userIdError') ?></small>
    </div>
    <div class="my-3">
        <button type="submit" class="btn btn-primary">Create</button>
    </div>
</form>