<div class="d-flex bd-highlight">
    <div class="p-2 flex-grow-1 bd-highlight">
        <h1><?= $title ?></h1>
    </div>
</div>

<table class="table">
    <thead>
    <tr>
        <th width="50">#</th>
        <th>Description</th>
        <th width="250">
            User
            <?= orderBy('name', $sort, $page); ?>
        </th>
        <th width="250">
            Email
            <?= orderBy('email', $sort, $page); ?>
        <th width="120">
            Status
            <?= orderBy('status', $sort, $page); ?>
        </th>
    </tr>
    </thead>
    <tbody>

    <?php foreach ($tasks['data'] as $key => $user) : ?>
        <tr>
            <td><?= $user['id']; ?></td>
            <td>
                <div class="task-text">
                    <a href="/tasks/<?= $user['id'] ?>/show"><?= $user['text']; ?></a>
                </div>
            </td>
            <td><?= $user['name']; ?></td>
            <td><?= $user['email']; ?></td>
            <td>
                <?php if ($user['status']) : ?>
                    <i class="fas fa-check"></i>
                <?php else : ?>
                    <i class="fas fa-minus"></i>
                <?php endif; ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<?php if ($tasks['pages'] > 1) : ?>
    <nav aria-label="Page navigation example">
        <ul class="pagination">
            <?php for ($i = 1; $i < ($tasks['pages'] + 1); $i++) : ?>
                <li class="page-item <?php echo $i == $page ? 'active' : '' ?>">
                    <a class="page-link" href="/tasks?sort=<?= $sort ?>&page=<?= $i ?>"><?= $i ?></a>
                </li>
            <?php endfor ?>
        </ul>
    </nav>
<?php endif; ?>

<a href="/tasks/create">Add task</a>
