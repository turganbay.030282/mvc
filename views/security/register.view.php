<div class="register">
    <div class="form-wrap">
        <h3>Register</h3>
        <form method="post" action="/register">
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" name="name" class="form-control" id="name">
                <small class="text-danger form-text"><?= getErrorMessage('nameError') ?></small>
            </div>

            <div class="form-group">
                <label for="login">Login</label>
                <input type="text" name="login" class="form-control" id="login">
                <small class="text-danger form-text"><?= getErrorMessage('loginError') ?></small>
            </div>

            <div class="form-group">
                <label for="email">Email</label>
                <input type="text" name="email" class="form-control" id="email">
                <small class="text-danger form-text"><?= getErrorMessage('emailError') ?></small>
            </div>

            <div class="form-group mb-2">
                <label for="password">Password</label>
                <input type="password" name="password" class="form-control" id="password">
                <small class="text-danger form-text"><?= getErrorMessage('passwordError') ?></small>
            </div>

            <div class="form-group mb-2">
                <label for="confirmPassword">Confirm password</label>
                <input type="password" name="confirmPassword" class="form-control" id="confirmPassword">
                <small class="text-danger form-text"><?= getErrorMessage('confirmPasswordError') ?></small>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>




