<div class="register">
    <div class="form-wrap">
        <h3>Login</h3>
        <form method="post" action="/login">
            <small class="text-danger form-text"><?= getErrorMessage('credentialsError') ?></small>
            <div class="form-group">
                <label for="login">Login</label>
                <input type="text" name="login" class="form-control" id="login" aria-describedby="loginHelp">
                <small class="text-danger form-text"><?= getErrorMessage('loginError') ?></small>
            </div>

            <div class="form-group mb-2">
                <label for="password">Password</label>
                <input type="password" name="password" class="form-control" id="password">
                <small class="text-danger form-text"><?= getErrorMessage('passwordError') ?></small>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>