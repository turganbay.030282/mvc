<?php

function view($path, $data = [], $baseTemplate = '')
{
    extract($data, EXTR_SKIP);
    $template = '../views/' . $path . '.view.php';

    if ($baseTemplate){
        return require '../views/'.$baseTemplate.'/base.view.php';
    }
    return require '../views/base.view.php';
}

function redirect($uri)
{
    header('location: ' . $uri);
    return null;
}

function dd($params)
{
    echo '<pre>';
    var_dump($params);
    echo '</pre>';
    exit(1);
}

function getErrorMessage($field)
{
    if (array_key_exists($field, $_SESSION)){
        $message = $_SESSION[$field];
        unset($_SESSION[$field]);
        return $message;
    }
    return '';
}

function orderBy($name, $sort, $page)
{
    if ($sort == $name . '-asc') {
        return '<a href="/tasks?sort=' . $name . '-desc&page='. $page.'"><i>▲</i></a>';
    } elseif ($sort == $name . '-desc') {
        return '<a href = "/tasks?sort=' . $name . '-asc&page='. $page.'"><i>▼</i></a>';
    } else {
        return '<a href="/tasks?sort=' . $name . '-asc&page='. $page.'"><i>▲▼</i></a>';
    }
}
