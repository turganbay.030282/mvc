<?php

return [
    'Database' => [
        'driver' => $_ENV['DB_DRIVER'],
        'host' => $_ENV['DB_HOST'],
        'dbname' => $_ENV['DB_NAME'],
        'username' => $_ENV['DB_USER'],
        'password' => $_ENV['DB_PASSWORD'],
        'options' => [
        ]
    ],
    'Middleware' => [
        'admin' => \Task\Middleware\AdminMiddleware::class
    ]
];