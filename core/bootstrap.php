<?php

use Task\App;
use Task\Database\Connection;
use Task\Database\DB;

try {

    $dotenv = Dotenv\Dotenv::createImmutable( '../');
    $dotenv->load();

    App::bind('config', require('../core/config.php'));
    App::bind(DB::class, new DB(Connection::make(App::get('config')['Database'])));

} catch (Exception $e) {
    die($e->getMessage());
}
