<?php


namespace Task\Middleware;

use Task\Request;

class AdminMiddleware
{
    public static function handle()
    {
        if (!Request::isAdmin()){
           redirect('/login');
        }
    }
}