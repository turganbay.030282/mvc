<?php

namespace Task;

use Exception;

class App
{
    protected static $registry = [];

    public static function bind($name, $data)
    {
        self::$registry[$name] = $data;
    }

    public static function get($name){
        if (!array_key_exists($name, self::$registry)){
            throw new Exception("The $name container not found");
        }

        return self::$registry[$name];
    }
}