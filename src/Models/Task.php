<?php

namespace Task\Models;

use Task\App;
use Task\Database\DB;

class Task
{
    public $id;
    public $name;

    public static function create(array $data)
    {
        $taskId = App::get(DB::class)->create('tasks', [
            'text' => $data['text'],
            'status' => $data['status'] ?? 0
        ]);
        $userId = $data['userId'] ?? null;
        if ($userId) {
            $userTaskData = ['task_id' => $taskId, 'user_id' => $userId];
            App::get(DB::class)->create('users_tasks', $userTaskData);
        }
    }

    public static function paginate(int $page, int $perPage, $sort = '')
    {
        $orderBy = "name asc";
        if ($sort) {
            $sort = explode('-', $sort);
            $orderField = $sort[0];
            $orders = ["name", "email", "status"];
            $key = array_search($orderField, $orders, true);
            $orderField = isset($orders[$key]) ? $orders[$key] : 'name';
            $type = $sort[1] === 'asc' ? 'asc' : 'desc';
            $orderBy = $orderField . ' ' . $type;
        }

        $offset = $perPage * ($page - 1);
        $count = App::get(DB::class)->count('tasks');
        $sql = sprintf("
            select tasks.id     id,
                   tasks.text   text,
                   tasks.status status,
                   ut.user_id   userId,
                   u.email      email,
                   u.name       name
            from tasks
             left join users_tasks ut on tasks.id = ut.task_id
             left join users u on ut.user_id = u.id
             order by %s 
             limit %d offset %d
        ", $orderBy, $perPage, $offset);

        return [
            'data' => App::get(DB::class)->fetchAll($sql),
            'pages' => ceil($count / $perPage),
        ];
    }

    public static function update($id, array $data)
    {
        $result = App::get(DB::class)->update('tasks', ['id' => $id], [
            'text' => $data['text'],
            'status' => $data['status'] ?? 0
        ]);

        if ($result) {
            App::get(DB::class)->delete('users_tasks', ['task_id' => $id]);
            $userId = $data['userId'] ?? null;
            if ($userId) {
                App::get(DB::class)->create('users_tasks', [
                    'task_id' => $id,
                    'user_id' => $userId
                ]);
            }
        }
    }

    public static function findWithUser($id)
    {
        $sql = "
            select tasks.id     id,
                   tasks.text   text,
                   tasks.status status,
                   ut.user_id   userId,
                   u.email      email,
                   u.name       name
            from tasks
             left join users_tasks ut on tasks.id = ut.task_id
             left join users u on ut.user_id = u.id
            where tasks.id = :id
        ";

        return App::get(DB::class)->row($sql, ['id' => $id]);
    }

    public static function delete($id)
    {
        App::get(DB::class)->delete('users_tasks', ['task_id' => $id]);
        return App::get(DB::class)->delete('tasks', ['id' => $id]);
    }
}