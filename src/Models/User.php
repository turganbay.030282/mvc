<?php

namespace Task\Models;

use Task\App;
use Task\Database\DB;

class User
{
    public static function paginate(int $page, int $perPage = 5): array
    {
        $offset = $perPage * ($page - 1);
        $count = App::get(DB::class)->count('users');
        $sql = sprintf("select * from users limit %d offset %d", $perPage, $offset);
        return [
            'data' => App::get(DB::class)->fetchAll($sql),
            'pages' => ceil($count / $perPage),
        ];
    }

    public static function findById($id)
    {
        return App::get(DB::class)->findByOne('users', ['id' => $id]);
    }

    public static function findByOne($data)
    {
        return App::get(DB::class)->findByOne('users', $data);
    }

    public static function update(array $data, $id)
    {
        if (!$data['password']) {
            unset($data['password']);
        } else {
            $data['password'] = md5($data['password']);
        }
        return App::get(DB::class)->update('users', ['id' => $id], $data);
    }

    public static function delete($id)
    {
        return App::get(DB::class)->delete('users', ['id' => $id]);
    }

    public static function create(array $data)
    {
        App::get(DB::class)->create('users', $data);
    }

    public static function all()
    {
        return App::get(DB::class)->all('users');
    }
}