<?php

namespace Task\Database;

use PDO;
use PDOException;

class Connection
{
    public static function make($config)
    {
        try {
            return new PDO(sprintf('%s:host=%s;dbname=%s', $config['driver'], $config['host'], $config['dbname']), $config['username'], $config['password']);
        } catch (PDOException $exception) {
            die($exception->getMessage());
        }
    }

}