<?php

namespace Task\Database;

use PDO;

class DB
{
    /**
     * @var PDO
     */
    private $pdo;

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * @throws \Exception
     */
    public function create($table, $params)
    {
        $fields = implode(', ', array_keys($params));
        $values = ':' . implode(', :', array_keys($params));

        $statement = $this->pdo->prepare(
            sprintf('insert into %s(%s) values(%s)', $table, $fields, $values)
        );

        if (!$statement->execute($params)) {
            throw new \Exception(json_encode($statement->errorInfo(), true), 500);
        }

        return $this->pdo->lastInsertId();
    }

    public function all($table)
    {
        $statement = $this->pdo->query('select * from ' . $table);
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function fetchAll(string $sql, $params = [])
    {
        $statement = $this->pdo->prepare($sql);
        if ($params){
            $this->setParams($params, $statement);
        }

        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function count(string $table)
    {
        $result = $this->pdo->query("SELECT count(*) FROM $table");
        return $result->fetchColumn();
    }

    public function findByOne($table, $params)
    {
        $statement = $this->pdo->prepare(sprintf("select * from %s %s limit 1", $table, $this->setConditions($params)));
        $this->setParams($params, $statement);
        $statement->execute();
        return $statement->fetch(PDO::FETCH_ASSOC);
    }


    public function row($sql, $params)
    {
        $statement = $this->pdo->prepare($sql);
        $this->setParams($params, $statement);
        $statement->execute();
        return $statement->fetch(PDO::FETCH_ASSOC);
    }


    public function update($table, $params, $data)
    {
        $values = '';
        foreach ($data as $key => $value) {
            $values .= (bool)$values ? ', ' : '';
            $values .= $key . ' = :' . $key;
        }

        $statement = $this->pdo->prepare(
            sprintf("update %s set %s %s", $table, $values, $this->setConditions($params))
        );

        $this->setParams(array_merge($params, $data), $statement);

        return $statement->execute();
    }

    public function delete($table, $params)
    {
        $statement = $this->pdo->prepare(
            sprintf("delete from %s %s", $table, $this->setConditions($params))
        );

        $this->setParams($params, $statement);

        return $statement->execute();
    }

    /**
     * @param $params
     * @param \PDOStatement $statement
     */
    private function setParams($params, \PDOStatement $statement)
    {
        foreach ($params as $key => $value) {
            $statement->bindValue(':' . $key, $value, PDO::PARAM_STR);
        }
    }

    /**
     * @param $params
     * @return string
     */
    private function setConditions($params)
    {
        $where = '';
        if (count($params)) {
            foreach ($params as $key => $v) {
                if ($where) {
                    $where .= ' and ';
                } else {
                    $where .= ' where ';
                }
                $where .= $key . ' = :' . $key . ' ';
            }
        }

        return $where;
    }
}
