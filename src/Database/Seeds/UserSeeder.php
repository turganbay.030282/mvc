<?php


use Phinx\Seed\AbstractSeed;

class UserSeeder extends AbstractSeed
{
    public function run()
    {
        $data = [
            [
                'name' => 'admin',
                'login' => 'admin',
                'email' => 'admin@gmail.com',
                'password' => md5(123),
                'role' => 'ROLE_ADMIN'
            ],
            [
                'name' => 'Owen',
                'login' => 'owen',
                'email' => 'perez@gmail.com',
                'password' => md5(123),
                'role' => 'ROLE_ADMIN'
            ],
            [
                'name' => 'Brinley',
                'login' => 'brinley',
                'email' => 'gray@gmail.com',
                'password' => md5(123),
                'role' => 'ROLE_ADMIN'
            ],
            [
                'name' => 'Diego',
                'login' => 'diego',
                'email' => 'gonzalez@gmail.com',
                'password' => md5(123),
                'role' => 'ROLE_ADMIN'
            ],
            [
                'name' => 'Simon',
                'login' => 'simon',
                'email' => 'brooks@gmail.com',
                'password' => md5(123),
                'role' => 'ROLE_ADMIN'
            ],
        ];

        $user = $this->table('users');
        $user->insert($data)->save();
    }
}
