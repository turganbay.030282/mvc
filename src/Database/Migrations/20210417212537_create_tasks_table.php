<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateTasksTable extends AbstractMigration
{
    public function up()
    {
        $this->execute('create table if not exists tasks(
                    id      int auto_increment primary key,
                    text    longtext not null,
                    status  tinyint(1) default 0 null
                );');
    }


    public function down()
    {
        $this->execute('DROP TABLE tasks');
    }
}
