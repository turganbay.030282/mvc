<?php
declare(strict_types=1);

//https://sergeivl.ru/phinx-migration.html

use Phinx\Migration\AbstractMigration;

final class CreateUsersTable extends AbstractMigration
{
    public function up()
    {
        $this->execute('create table if not exists users(
                    id       int auto_increment primary key,
                    name     varchar(255) null,
                    login    varchar(255) not null,
                    email    varchar(255) not null,
                    password varchar(255) null,
                    role     varchar(255) null,
                    constraint user_login_uindex unique (login),
                    constraint user_email_uindex unique (email)
                );');
    }

    public function down()
    {
        $this->execute('DROP TABLE users');
    }
}
