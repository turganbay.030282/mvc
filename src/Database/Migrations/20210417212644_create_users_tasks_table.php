<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateUsersTasksTable extends AbstractMigration
{

    public function up()
    {
        $this->execute(
            'create table if not exists users_tasks
                    (
                        user_id int not null,
                        task_id int not null,
                        constraint task_id foreign key (task_id) references tasks (id),
                        constraint user_id foreign key (user_id) references users (id)
                    );
                );'
        );
    }

    public function down()
    {
        $this->execute('DROP TABLE users_tasks');
    }
}
