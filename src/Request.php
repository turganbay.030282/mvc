<?php

namespace Task;


class Request
{
    public static function uri()
    {
        $uri = $_SERVER['REQUEST_URI'];
        $uri = urldecode(preg_replace('/\?.*/iu', '', $uri));
        return trim($uri, '/');
    }
    public static function method()
    {
        return $_SERVER['REQUEST_METHOD'];
    }

    public static function get($name)
    {
        if (array_key_exists($name, $_REQUEST)){
            return $_REQUEST[$name];
        }
        return null;
    }

    public static function isAdmin()
    {
        return isset($_SESSION['userId'])
            && isset($_SESSION['userName'])
            && isset($_SESSION['userLogin'])
            && isset($_SESSION['userRole'])
            && $_SESSION['userRole'] === 'ROLE_ADMIN';
    }
}
