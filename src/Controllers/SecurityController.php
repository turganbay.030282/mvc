<?php


namespace Task\Controllers;


use Task\Models\User;
use Task\Request;

class SecurityController
{
    public function registerForm()
    {
        return view('security/register');
    }

    public function register()
    {
        $valid = true;
        if (!$name = Request::get('name')) {
            $_SESSION['nameError'] = 'Name is required';
            $valid = false;
        }
        if (!$email = Request::get('email')) {
            $_SESSION['emailError'] = 'Email is required';
            $valid = false;
        }
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $_SESSION['emailError'] = "Invalid email format";
        }
        if (!$login = Request::get('login')) {
            $_SESSION['loginError'] = 'Login is required';
            $valid = false;
        }

        if (!$password = Request::get('password')) {
            $_SESSION['passwordError'] = 'Password is required';
            $valid = false;
        } else if ($password !== Request::get('confirmPassword')) {
            $_SESSION['confirmPasswordError'] = 'Passwords must match.';
            $valid = false;
        }

        if (!$valid) {
            return view('security/register');
        }

        $password = md5($password);
        try {
            User::create(compact('name', 'email', 'login', 'password'));
            $_SESSION['success'] = 'You have successfully registered';
            return redirect('/');
        } catch (\Exception $exception) {
            if (preg_match('#user_login_uindex#', $exception->getMessage(), $matches)) {
                $_SESSION['loginError'] = 'A user with this login is already registered.';
            } else {
                $_SESSION['error'] = $exception->getMessage();
            }
            return view('security/register');
        }
    }

    public function loginForm()
    {
        return view('security/login');
    }

    /**
     * @throws \Exception
     */
    public function login()
    {
        $login = Request::get('login');
        $password = Request::get('password');

        $valid = true;
        if (!$password) {
            $_SESSION['passwordError'] = 'Password is required';
            $valid = false;
        }
        if (!$login) {
            $_SESSION['loginError'] = 'Login is required';
            $valid = false;
        }

        if (!$valid) {
            return view('security/login');
        }

        $password = md5($password);

        $user = User::findByOne(compact('login', 'password'));

        if (!$user) {
            $_SESSION['credentialsError'] = 'Invalid credentials.';
            return view('security/login');
        }

        $_SESSION['userId'] = $user['id'];
        $_SESSION['userName'] = $user['name'];
        $_SESSION['userLogin'] = $user['login'];
        $_SESSION['userRole'] = $user['role'];
            return redirect('/');
    }

    public function logout()
    {
        unset($_SESSION['userId']);
        unset($_SESSION['userName']);
        unset($_SESSION['userLogin']);
        unset($_SESSION['userRole']);
        return redirect('/');
    }
}