<?php


namespace Task\Controllers\Admin;

use Task\Models\User;
use Task\Request;

class UserController
{
    public function index()
    {
        $title = 'Users';
        $page = Request::get('page') ?? 1;
        $users = User::paginate($page);
        return view('admin/users/index', compact('users', 'title'), 'admin');
    }

    public function create()
    {
        $title = 'Create User';
        $action = '/admin/users/store';
        return view('admin/users/form', compact('title', 'action'), 'admin');
    }

    public function store()
    {
        $valid = true;
        if (!$name = Request::get('name')) {
            $_SESSION['nameError'] = 'Name is required';
            $valid = false;
        }
        if (!$email = Request::get('email')) {
            $_SESSION['emailError'] = 'Email is required';
            $valid = false;
        }
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $_SESSION['emailError'] = "Invalid email format";
        }
        if (!$login = Request::get('login')) {
            $_SESSION['loginError'] = 'Login is required';
            $valid = false;
        }

        if (!$password = Request::get('password')) {
            $_SESSION['passwordError'] = 'Password is required';
            $valid = false;
        }

        if (!$valid){
            return redirect('/admin/users/create');
        }

        $password = md5($password);
        try {
            User::create(compact('name', 'email', 'login', 'password'));
            $_SESSION['success'] = 'User created';
            return redirect('/admin/users');
        } catch (\Exception $exception){
            $_SESSION['error'] = $exception->getMessage();
            return redirect('/admin/users/create');
        }
    }

    public function edit($id)
    {
        $user = User::findById($id);
        $title = 'Edit User';
        $action = '/admin/users/'.$id.'/update';
        return view('admin/users/form', compact('title', 'user', 'action'), 'admin');
    }

    public function update($id)
    {
        User::update([
            'name' => Request::get('name'),
            'email' => Request::get('email'),
            'login' => Request::get('login'),
            'password' => Request::get('password'),
        ], $id);
        return redirect("/admin/users");
    }

    public function delete($id)
    {
        User::delete($id);
        return redirect("/admin/users");
    }
}