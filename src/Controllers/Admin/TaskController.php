<?php

namespace Task\Controllers\Admin;

use Task\Models\Task;
use Task\Models\User;
use Task\Request;

class TaskController
{
    public function index()
    {
        $title = 'Tasks';
        $page = Request::get('page') ?? 1;
        $tasks = Task::paginate($page, 3);
        return view('admin/tasks/index', compact('tasks', 'title'), 'admin');
    }

    public function edit($id)
    {
        $users = User::all();
        $task = Task::findWithUser($id);
        return view('admin/tasks/edit', compact('task', 'users'), 'admin');
    }

    public function update($id)
    {
        $valid = true;
        $userId = Request::get('userId');

        $status = (bool)Request::get('status') ? 1 : 0;
        if (!$text = Request::get('text')) {
            $_SESSION['textError'] = 'Description is required';
            $valid = false;
        }

        if (!$valid) {
            return redirect('/admin/users/' . $id . '/edit');
        }

        try {
            Task::update($id, compact('text', 'userId', 'status'));
            $_SESSION['success'] = 'Task updated';
            return redirect('/admin/tasks');
        } catch (\Exception $exception) {
            $_SESSION['error'] = $exception->getMessage();
            return redirect('/admin/users/' . $id . '/edit');
        }
    }

    public function delete($id)
    {
        if (Task::delete($id)) {
            $_SESSION['success'] = 'Task deleted';
        } else {
            $_SESSION['error'] = 'Not deleted';
        }

        return redirect("/admin/tasks");
    }
}