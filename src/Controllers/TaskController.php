<?php

namespace Task\Controllers;

use Task\App;
use Task\Database\DB;
use Task\Models\Task;
use Task\Models\User;
use Task\Request;

class TaskController
{
    public function index()
    {
        $title = 'Tasks';
        $page = Request::get('page') ?? 1;
        $sort = Request::get('sort') ?? '';
        $tasks = Task::paginate($page, 3, $sort);
        return view('home', compact('tasks', 'title', 'page', 'sort'));
    }

    public function create()
    {
        $users = User::all();
        return view('tasks/create', compact('users'));
    }

    public function store()
    {
        $valid = true;
        if (!$text = Request::get('text')) {
            $_SESSION['textError'] = 'Description is required';
            $valid = false;
        }
        if (!$userId = Request::get('userId')) {
            $_SESSION['userIdError'] = 'User is required';
            $valid = false;
        }

        if (!$valid){
            $_SESSION['error'] = 'Invalid form';
            return redirect('/tasks/create');
        }
        try {
            Task::create(compact('text', 'userId'));
            $_SESSION['success'] = 'Task created';
            return redirect('/');
        } catch (\Exception $exception){
            $_SESSION['error'] = $exception->getMessage();
            return redirect('/tasks/create');
        }
    }

    public function show($id)
    {
        $task = Task::findWithUser($id);
        return view('tasks/show', compact('task'));
    }
}