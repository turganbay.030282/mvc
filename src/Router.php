<?php

namespace Task;

use Exception;

class Router
{
    protected static $routes = [
        'GET' => [],
        'POST' => [],
    ];

    private $uri = '';

    public static function get($uri, $options)
    {
        self::$routes['GET'][trim($uri, '/')] = $options;
    }

    public static function post($uri, $options)
    {
        self::$routes['POST'][trim($uri, '/')] = $options;
    }

    public static function loadRoutes($path)
    {
        require $path;
        return new static();
    }

    public function direct($uri)
    {
        $this->uri = $uri;
        try {
            $this->callAction(self::$routes[Request::method()]);
        } catch (Exception $exception) {
            $errorMessage = $exception->getMessage();
            $errorCode = $exception->getCode();
            http_response_code($errorCode);
            switch ($errorCode) {
                case 404:
                    return view('errors/404', [
                        'message' => $errorMessage,
                        'code' => $errorCode,
                    ]);
                case 500:
                    return view('errors/500', [
                        'message' => $errorMessage,
                        'code' => $errorCode,
                    ]);
                default:
                    return redirect('login');
            }
        }
    }


    /**
     * @throws Exception
     */
    private function callAction($data)
    {
        $params = [];
        if (!array_key_exists($this->uri, $data)) {
            $params = $this->checkDynamicUrl($data);
        }

        if (!array_key_exists($this->uri, $data)) {
            throw new Exception(sprintf('Route %s is not defined', $this->uri), 404);
        }

        if (array_key_exists('middleware', $data[$this->uri])) {
            $middlewareName = $data[$this->uri]['middleware'];
            $middlewareClass = App::get('config')['Middleware'][$middlewareName];
            $middlewareClass::handle();
        }

        $controllerName = $data[$this->uri]['controller'];
        $action = $data[$this->uri]['action'];
        $controller = new $controllerName;

        if (!method_exists($controller, $action)) {
            throw new Exception(sprintf('Method %s::%s not found', $controllerName, $action), 404);
        }

        $controller->$action(...$params);
    }


    private function checkDynamicUrl($data)
    {
        $partsFromUri = explode('/', $this->uri);
        $partsFromUriCount = count($partsFromUri);
        $params = [];

        foreach ($data as $route => $value) {
            $partsFromData = explode('/', $route);

            if ($partsFromUriCount === count($partsFromData)) {
                $newUri = [];
                $matches = true;
                for ($i = 0; $i < $partsFromUriCount; $i++) {
                    if ($partsFromUri[$i] === $partsFromData[$i]) {
                        $newUri[] = $partsFromData[$i];
                    } else if (preg_match('/\{(.*?)\}/', $partsFromData[$i])) {
                        $params[] = $partsFromUri[$i];
                        $newUri[] = $partsFromData[$i];
                    } else {
                        $matches = false;
                    }
                }
                if ($matches) {
                    $this->uri = implode('/', $newUri);
                    break;
                }
            }
        }

        return $params;
    }
}

